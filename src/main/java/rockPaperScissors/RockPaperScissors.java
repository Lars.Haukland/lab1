package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
    	while(true) {
    		// Init output starting new round
    		System.out.println("Let's play round " + roundCounter);
    		
    		// Get user choice
    		String userChoice = getUserChoice();
    		
    		// Get computer choice
    		String computerChoice = getComputerChoice();
        	
        	// Get result from round and print it
    		String outcome; // String for outcome example "Human wins!"
        	if(isWinner(userChoice, computerChoice)) {
        		// User wins the round
        		outcome = "Human wins!";
        		humanScore++;
        	}else if (isWinner(computerChoice, userChoice)) {
        		// Computer wins the round
        		outcome = "Computer Wins!";
        		computerScore++;
        	}else {
        		// No one wins => its a tie
        		outcome = "It's a tie!";
        	}
        	roundCounter++;
        	
        	// Output round result
        	System.out.println("Human chose " + userChoice + ", computer chose " + computerChoice + ". " + outcome);
        	
        	System.out.println("Score: human " + humanScore + ", computer " + computerScore);
        	
        	
        	// Prompt if user wants to continue
        	String userContinue = readInput("Do you wish to continue playing? (y/n)?");
        	
        	if (userContinue.equals("n")) break;
    	}
    	System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    
    public boolean isWinner(String choice1, String choice2) {
    	if (choice1.equals("rock")) {
    		return choice2.equals("scissors");
    	}else if (choice1.equals("paper")) {
    		return choice2.equals("rock");
    	}else {
    		return choice2.equals("paper");
    	}
    }
    
    public String getUserChoice() {
    	String choice;
    	
    	// Get user input and return
    	choice = readInput("Your choice (Rock/Paper/Scissors)?");
    	
    	return choice;
    }
    
    public String getComputerChoice() {
    	// Highest random number
    	int randUpper = 2;
    	// Init new random object
    	Random rand = new Random();
    	// Get random integer
    	int randInt = rand.nextInt(randUpper);
    	// Use random integer to get choice
    	String computerChoice = rpsChoices.get(randInt);
    	
    	// Return choice
    	return computerChoice;
    }

}
